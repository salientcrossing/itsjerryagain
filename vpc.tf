# Create a VPC
resource "aws_vpc" "demo" {
  cidr_block = var.cidr_block
  tags = {
    "Name" = "Demo"
  }
}