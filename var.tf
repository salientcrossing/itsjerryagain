variable "cidr_block" {
  default = "10.0.0.0/16"

}

variable "demo" {
  type    = string
  default = "demo_vpc"

}

variable "aws_internet_gateway" {
  type    = string
  default = "demo_internet_gw"

}
variable "vpc_id" {
  type    = string
  default = "demo"

}

variable "aws_subnet-public" {
  type    = list(string)
  default = ["10.0.1.0/24", "10.0.9.0/24"]

}
# variable "aws_subnet-public" {
#     count = 2
#     vpc_id = var.vpc_cidr
#     cidr_block = "${cidr_subnet(var.vpc_cidr,8,count.index)}"

#     tags = {
#         "name" = "subnet ${count.index + 0}"    }

# }

variable "aws_subnet-private" {
  type    = list(string)
  default = ["10.0.3.0/24", "10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"]

}
# variable "aws_subnet-private" {
#     count = 4
#     vpc_id = var.vpc_cidr
#     cidr_block = "${cidr_subnet(var.vpc_cidr,8,count.index)}"

#     tags = {
#         "name" = "subnet ${count.index + 0}"    }

# }

variable "ingress_rule" {
  type    = list(string)
  default = ["22", "80", "3306", "443", "8080"]

}

# variable "egress_rule" {
#     type = number
#     default =["0.0.0.0/0"]

# }

variable "sub_az" {
  type    = list(string)
  default = ["us-east-1a", "us-east-1b", "us-east-1c", "us-east-1d"]

}