
resource "aws_route_table" "demo_rt" {
  count  = 2
  vpc_id = aws_vpc.demo.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gw.id
    # network_interface_id = element(aws_network_interface.application.*.id, count.index)
  }

  tags = {
    Name = "demo_rt[count.index]"
  }
}


#this resource is giving me some trouble aroud the subnet id
resource "aws_route_table_association" "a" {
  count          = 4
  subnet_id      = element(aws_subnet.private[*].id, count.index)
  route_table_id = element(aws_route_table.demo_rt[*].id, count.index)
}

resource "aws_route_table_association" "b" {
  count          = 2
  subnet_id      = aws_subnet.pub[count.index].id
  route_table_id = aws_route_table.demo_rt[count.index].id
}


