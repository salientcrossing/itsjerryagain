resource "aws_network_interface" "demo_eni" {
  count     = 2
  subnet_id = element(aws_subnet.pub.*.id, count.index)
  #   private_ips     = ["10.0.0.50"]
  security_groups = [aws_security_group.allow_tls.id]

  attachment {
    instance     = element(aws_instance.demo_instance_1[*].id, count.index)
    device_index = "1"
  }

  tags = {
  "name" = "subnet ${count.index + 0}" }
}