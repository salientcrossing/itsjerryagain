resource "aws_db_instance" "rds" {
  count = 4

  allocated_storage      = 10
  db_name                = "rara"
  engine                 = "mysql"
  engine_version         = "5.7"
  instance_class         = "db.t3.micro"
  username               = "foo"
  password               = "foobarbaz"
  # parameter_group_name   = "default.mysql5.7"
  skip_final_snapshot    = true
  vpc_security_group_ids = flatten([aws_security_group.allow_tls.id])
  db_subnet_group_name   = aws_db_subnet_group.default[count.index].name
  #   skip_final_snapshot_identifier = true
}